#!/usr/bin/perl

# Begin-Doc
# Name: gen-indexes.pl
# Description: generate index pages for files/dirs in a tree
# End-Doc

use lib "/local/perllib/libs";
use lib "/local/mstperl/libs";
use MST::AppTemplate;
use Local::HTMLUtil;

use File::Path;
use Getopt::Long;
use File::Find;
use strict;

umask(022);

my $help;
my $debug;
my $dir;

my $res = GetOptions(
    "debug+" => \$debug,
    "help+"  => \$help,
    "dir=s" => \$dir,
);
if ( !$res || $help || !$dir ) {
    print "Usage: $0 [--debug] [--help] --dir dir\n";
    exit(1);
}
print "Processing dir ($dir).\n";

chdir($dir) || die;

File::Find::find( { wanted => \&process_file }, $dir );

# Begin-Doc
# Name: process_file
# Description: callback for recursive find, processes each file
# End-Doc

sub process_file {
    my $fname = $_;
    my $dir   = $File::Find::dir;
    my $path  = $File::Find::name;

    if ( $fname eq ".git" ) {
        $File::Find::prune = 1;
        return;
    }

    $debug && print $path, "\n";

    if ( ! -d $path ) {
        print "Not a dir ($path), skipping.\n";
        return;
    }

    my $idxfile = $path . "/index.html";
    print "Processing $dir // $path // $fname   into $idxfile\n";

    opendir(my $subdir, $path);
    my @links = ();
    push(@links, "<a href=\"..\">Up one level</a><br>");

    while ( defined(my $entry = readdir($subdir)) ) {
        print "  found $entry\n";
        next if ( $entry eq "." || $entry eq ".." );
        next if ( $entry eq "index.html" );

        if ( -f "$path/$entry" ) {
            my $bname = $entry;
            $bname =~ s/\.html$//go;

            push(@links, "<a href=\"$entry\">$bname</a>");
        } else {
            push(@links, "<a href=\"$entry\">$entry/</a>");
        }
    }

    my $html = new MST::AppTemplate(
    title           => "MSTDoc Perl Library Documentation",
    );
    &HTMLSentContentType(1);

    open(my $out, ">$idxfile");
    select($out);
    $html->PageHeader();

    print $out "<ul>\n";
    foreach my $link ( sort @links ) {
        print $out "<li>$link\n";
    }
    print $out "</ul>\n";

    $html->PageFooter();
    close($out);
    select(STDOUT);
}
