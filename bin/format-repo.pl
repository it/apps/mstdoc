#!/usr/bin/perl

# Begin-Doc
# Name: format-repo.pl
# Description: scan a single repo for published docs
# End-Doc

use lib "/local/perllib/libs";
use lib "/local/mstperl/libs";
use lib "/local/mstdoc/libs";
use MSTDoc;
use File::Path;
use Getopt::Long;
use File::Find;
use strict;

umask(022);

my $help;
my $debug;
my $fromdir;
my $todir;
my $url;

my $res = GetOptions(
    "debug+" => \$debug,
    "help+"  => \$help,
    "from=s" => \$fromdir,
    "to=s"   => \$todir,
    "url=s" => \$url,
);
if ( !$res || $help || !$fromdir || !$todir ) {
    print "Usage: $0 [--debug] [--help] --from dir --to dir [--url baseurl]\n";
    exit(1);
}
print "Processing from ($fromdir) to ($todir).\n";

chdir($fromdir) || die;

# Sanity check
if ( !-e "./.git" ) {
    die "Not a git repo.\n";
}

File::Find::find( { wanted => \&process_file }, '.' );

# Begin-Doc
# Name: process_file
# Description: callback for recursive find, processes each file
# End-Doc

sub process_file {
    my $fname = $_;
    my $dir   = $File::Find::dir;
    my $path  = $File::Find::name;

    if ( $fname eq ".git" ) {

        $File::Find::prune = 1;
        return;
    }

    $debug && print $path, "\n";

    if ( -f $fname ) {
        $debug && print $dir, ": ", $fname, "\n";
        open( my $in, "<$fname" );
        my $data = join( "", <$in> );
        close($in);

        my $blocks = &MSTDoc_Parse($data);
        if ( !$blocks ) {
            $debug && print "Failed doc extract from: $path\n";
            next;
        }

        my @tmp = @{$blocks};
        if ( $#tmp < 0 ) {
            $debug && print "No doc found in: $path\n";
            next;
        }

        my $htmldir = $dir;
        $htmldir =~ s|^\./||go;
        my $htmlurl = $url . "/blob/master/" . $htmldir . "/" . $fname;
        my @urlopt;
        if ( $url )
        {
            push(@urlopt, "url" => $htmlurl);
        }

        my $html = &MSTDoc_Format(
            data  => $blocks,
            style => "html",
            @urlopt
        );

        my $dirpath = $todir . "/" . $dir;
        $debug && print "Generating html to: $dirpath/${fname}.html\n";
        if ( $dirpath ne "" ) {
            mkpath( $dirpath, 1, 0755 );
        }
        open( my $out, ">$dirpath/${fname}.html" );
        print $out $html;
        close($out);
    }
}
