
=begin
Begin-Doc
Name: mstdoc
Type: module
Description: Missouri S&T Documentation Standard utility functions
Comment: Contains various routines for extracting and parsing documentation
	blocks in the Missouri S&T standard format. 
Requires: no special permissions required to use this module, however, the
	mstdoc command is provided already, so unless you are writing a
	special formatter, you shouldn't need to use this module directly.
Doc: http://svn.mst.edu/~mstdoc
Doc-Standard: http://svn.mst.edu/~mstdoc/standard.html
Doc-Other: http://svn.mst.edu/~mstdoc/mstdoc.html
End-Doc
=cut

package MSTDoc;
require 5.000;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

use Data::Dumper;
use Cwd;
use File::Basename;
use File::Path;
use Local::UsageLogger;

@ISA    = qw(Exporter);
@EXPORT = qw(
    MSTDoc_LoadFile
    MSTDoc_Parse
    MSTDoc_Format
    MSTDoc_Search
);

BEGIN {
    &LogAPIUsage();
}

#
# Cache for cwd lookups
#
our %fast_abs_path_cache = ();

# Begin-Doc
# Name: MSTDoc_Parse
# Type: function
# Syntax: $arrayref = &MSTDoc_Parse($text);
# Description: Extracts documentation blocks from a string
#
# Comment: Returns a array ref containing the parsed contents of the
# documentation extracted from $text.
#
# End-Doc
sub MSTDoc_Parse {
    my ($text) = @_;
    my ( $block, $newblock, $blocks );

    $blocks = [];

    study $text;

    while ( $text =~ m|\bBegin-Doc\b(.*?)\bEnd-Doc\b|sgmio ) {
        $block    = $1;
        $newblock = &MSTDoc_CleanBlock($block);
        push( @$blocks, &MSTDoc_ParseBlock($newblock) );
    }
    return $blocks;
}

# Begin-Doc
#   Name: MSTDoc_ParseBlock
#   Type: function
#   Syntax: $arrayref = &MSTDoc_ParseBlock($block);
#   Description: Parses a single block and returns a array ref
#   Returns: array containing the parsed contents
#     of the block, array elements are references to hashes, containing
#     'attribute' and 'value'.
# End-Doc
sub MSTDoc_ParseBlock {
    my ($text) = @_;
    my $block = {};
    my ( $curattrib, $curvalue, $tmphash );
    my ( $attribute, $value,    $lastline );
    my ( $line,      $attrib,   $name, $type );

    $curattrib = "";
    $curvalue  = "";
    $lastline  = "";
    foreach $line ( split( /[\r\n]/, $text ) ) {

        # don't include doubled blank lines
        next if ( $line eq "" && $lastline eq "" );

        if ( $line =~ /^([a-z0-9\-]+):(.*)/io ) {
            $attrib = lc $1;
            $value  = $2;
            $value =~ s/^\s*//gio;
            $value =~ s/\s*$//gio;

            # common mistake
            if ( $attrib eq "comments" ) {

                # should issue warning here
                $attrib = "comment";
            }

            if ( $curattrib ne "" ) {
                if (   $curattrib ne "type"
                    && $curattrib ne "name"
                    && $curattrib ne "keywords"
                    && $curattrib ne "description"
                    && $curattrib ne "rcsid" )
                {
                    push(
                        @{ $block->{others} },
                        {   attribute => $curattrib,
                            value     => $curvalue
                        }
                    );
                }
                else {
                    $block->{$curattrib} = $curvalue;
                }
            }
            $curattrib = $attrib;
            $curvalue  = $value;
        }
        elsif ( $curattrib ne "" )    # unlabeled line, and active attrib
        {
            $value = $line;
            $value =~ s/^\s*//gio;
            $value =~ s/\s*$//gio;

            if ( $curvalue ne "" ) {
                $curvalue .= "\n";
            }
            $curvalue .= $value;
        }

        $lastline = $line;
    }

    if ( $curattrib ne "" ) {
        if (   $curattrib ne "type"
            && $curattrib ne "name"
            && $curattrib ne "keywords"
            && $curattrib ne "description"
            && $curattrib ne "rcsid" )
        {
            push(
                @{ $block->{others} },
                {   attribute => $curattrib,
                    value     => $curvalue
                }
            );
        }
        else {
            $block->{$curattrib} = $curvalue;
        }
    }

    return $block;
}

# Begin-Doc
#   Name: MSTDoc_CleanBlock
#   Type: function
#   Syntax: $cleanedblock = &MSTDoc_CleanBlock($block);
#   Description: Strips extraneous whitespace and comment characters
# End-Doc
sub MSTDoc_CleanBlock {
    my ($block) = @_;
    my ( $newblock, $lastline, $line );

    #
    # Handle any leading/trailing/doubled whitespace
    #
    $block =~ s|^\s+||gio;
    $block =~ s|\s+$||gio;

    $newblock = "";
    $lastline = "";
    foreach $line ( split( /\n/, $block ) ) {

        # eliminate all commenting that we can detect
        $line =~ s|\s* \*+/ \s*$||sgmiox;
        $line =~ s|^\s* [\#\*\{]+ \s* ||sgmiox;
        $line =~ s|^\s* /\*+ \s* ||sgmiox;
        $line =~ s|^\s* // \s* ||sgmiox;
        $line =~ s|^\s* <!(--)* \s* ||sgmiox;
        $line =~ s|\s* (--)*!> \s*$||sgmiox;
        $line =~ s|^\s* -- ||sgmiox;
        $line =~ s|^\s* (\S+)   |$1|sgmiox;

        # special case for C/C++ code
        $line =~ s|\\n"$||sgmiox;
        $line =~ s|^"||sgmiox;

        next if ( $line eq "" && $lastline eq "" );
        $lastline = $line;
        $newblock .= $line . "\n";
    }
    $newblock =~ s|^\s+||gio;
    $newblock =~ s|\s+$||gio;

    return $newblock;
}

# Begin-Doc
# Name: MSTDoc_Format
# Type: function
# Description: Formats documentation, parses if not already parsed
# Syntax: $formattedtext = &MSTDoc_Format(data => $arrayref, style => $style);
# Syntax: $formattedtext = &MSTDoc_Format(text => $text, style => $style);
#
# Comment: returns the documentation formatted in a particular style.
# Only supports HTML at the moment. Ideally, should make this extensible. If
# you pass 'text', then it parses it first, otherwise it uses the already
# parsed version.
# End-Doc

sub MSTDoc_Format {
    my (%info) = @_;
    my ( $blocks, $style, $cmd, $output );

    $blocks = $info{data};
    if ( !$blocks ) {
        $blocks = &MSTDoc_Parse( $info{text} );
    }

    $style = $info{style};
    if ( $style eq "" || $style eq "dump" ) {
        return &MSTDoc_Format_DUMP( $blocks, %info );
    }
    elsif ( $style eq "html" ) {
        return &MSTDoc_Format_HTML( $blocks, %info );
    }
    else {
        die "Unknown formatter!\n";
    }
}

# Begin-Doc
# Name: MSTDoc_Format_DUMP
# Type: function
# Description: Formats documentation in Data::Dumper style
# Syntax: $formattedtext = &MSTDoc_Format_DUMP($blocks);
# End-Doc
sub MSTDoc_Format_DUMP {
    my ($blocks) = @_;
    return Data::Dumper->Dump( [$blocks], [qw(blocks)] );
}

# Begin-Doc
# Name: MSTDoc_Format_HTML
# Type: function
# Description: Formats documentation in HTML
# Syntax: $formattedtext = &MSTDoc_Format_HTML($blocks);
# End-Doc
sub MSTDoc_Format_HTML {
    my ( $blocks, %parms ) = @_;
    my $html = "";
    my ( $title, %info, $block );

    %info = &MSTDoc_ExtractInfo($blocks);

    # Should try to apptemplate'ize this generated HTML if possible

    $title = ucfirst $info{type} . ": " . $info{name};
    $html .= "<TITLE>$title</TITLE>\n";
    $html .= "<CENTER><H2>$title</H2>\n";
    if ( $parms{url} ) {
        $html
            .= "<CENTER>Original File: <A HREF=\""
            . $parms{url} . "\">"
            . $parms{url}
            . "</A></CENTER>\n";
    }
    if ( $info{rcsid} ) {
        $html
            .= "<FONT SIZE=-1>RCS ID: <TT>" . $info{rcsid} . "</TT></FONT>\n";
    }
    if ( $parms{revision} ) {
        $html
            .= "<FONT SIZE=-1>Original File Revision: <TT>"
            . $parms{revision}
            . "</TT></FONT>\n";
    }
    $html .= "</CENTER>\n";
    $html .= "<HR><P>\n";

    $html .= "<DL>\n";

    my $lastsection = "";
    foreach $block (@$blocks) {
        my $name     = $block->{name};
        my $type     = lc $block->{type};
        my $desc     = $block->{description};
        my $keywords = $block->{keywords};

        my $is_internal = 0;
        foreach my $entry ( @{ $block->{others} } ) {
            my $attrib = $entry->{attribute};
            my $value  = $entry->{value};
            if ( $attrib eq "access" && $value =~ /internal/i ) {
                $is_internal = 1;
            }
        }
        next if ($is_internal);

        $type =~ s/\s+$//gio;

        if ( $type eq "section" ) {
            $html .= "</DL><HR><DL>\n";
        }
        $html .= "<P><DT>";
        $html .= "<A NAME=\"$name\">";
        $html .= "<B><TT>";
        if ( $type eq "section" ) {
            $html .= "<FONT SIZE=+2>$name</FONT>";
        }
        else {
            $html .= "<FONT SIZE=+1>$name</FONT>";
        }
        $html .= "</TT></B> ($type): $desc\n";
        $html .= "<BR>\n";

        if ( $keywords ne "" ) {
            $html .= "<DD>Keywords: <TT>$keywords</TT>\n";
        }

        foreach my $entry ( @{ $block->{others} } ) {
            my $attrib = $entry->{attribute};
            my $value  = $entry->{value};
            $value =~ s|\n\n|<P>|gio;

            if ( $attrib eq "syntax" ) {
                $html .= "<DD>Syntax: <TT>$value</TT>\n";
            }
            elsif ( $attrib eq "doc" ) {
                $html .= "<DD>Doc: <A HREF=\"$value\">$value</A>\n";
            }
            elsif ( $attrib =~ /^doc-(.*)$/io ) {
                my $label = ucfirst $1;
                $html .= "<DD>Doc ($label): <A HREF=\"$value\">$value</A>\n";
            }
            elsif ( $attrib eq "email" ) {
                $html .= "<DD>EMail: <A HREF=\"mailto:$value\">$value</A>\n";
            }
            elsif ( $attrib eq "example" ) {
                my $tmp = $value;
                $tmp =~ s/\n/<BR>/gio;
                $html .= "<DD>Example Code:<BR><DL><DD>$tmp</DL>\n";
            }
            elsif ( $attrib ne "comment" ) {
                $html .= "<DD>" . ucfirst $attrib . ": $value\n";
            }
            else {
                $html .= "<P><DD>$value<P>\n";
            }
        }
    }
    $html .= "</DL>\n";

    $html .= "<P><HR>";
    $html .= "<FONT SIZE=-2>HTML Generated: " . scalar(localtime) . "\n";

    $html .= "</FONT>\n";

    return $html;
}

# Begin-Doc
# Name: MSTDoc_ExtractInfo
# Type: function
# Description: Tries to determine an overall title/description
# Syntax: %info = &MSTDoc_ExtractInfo($blocks);
# Comment:
# End-Doc
sub MSTDoc_ExtractInfo {
    my ($blocks) = @_;
    my ( $attribute, $block, $entry, $value, %info );

    foreach $block ( @{$blocks} ) {
        if (   $block->{type} eq "module"
            || $block->{type} eq "library"
            || $block->{type} eq "script"
            || $block->{type} eq "schema"
            || $block->{type} eq "file" )
        {
            return (
                name        => $block->{name},
                description => $block->{description},
                type        => $block->{type},
                rcsid       => $block->{rcsid},
                keywords    => $block->{keywords},
            );
        }
    }
    return undef;
}

# Begin-Doc
# 	Name: MSTDoc_LoadFile
# 	Type: function
# 	Description: load a file into a string
#   Comment: this really is overkill to include as a function in a module
#   but it makes loading the entire file/command as a string really easy.
# End-Doc
sub MSTDoc_LoadFile {
    my ($file) = @_;
    my ( $text, $save_sep );

    open( my $in, $file );
    $save_sep = $/;
    $/        = undef;
    $text     = join( "", <$in> );
    $/        = $save_sep;
    close($in);

    return $text;
}

1;
